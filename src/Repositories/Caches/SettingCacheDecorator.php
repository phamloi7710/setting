<?php

namespace LoiPham\Setting\Repositories\Caches;

use LoiPham\Setting\Repositories\Interfaces\SettingInterface;
use LoiPham\Support\Repositories\Caches\CacheAbstractDecorator;

class SettingCacheDecorator extends CacheAbstractDecorator implements SettingInterface
{

}
