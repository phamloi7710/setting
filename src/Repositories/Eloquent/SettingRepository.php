<?php

namespace LoiPham\Setting\Repositories\Eloquent;

use LoiPham\Setting\Repositories\Interfaces\SettingInterface;
use LoiPham\Support\Repositories\Eloquent\RepositoriesAbstract;

class SettingRepository extends RepositoriesAbstract implements SettingInterface
{
}
