<?php

namespace LoiPham\Setting\Repositories\Interfaces;

use LoiPham\Support\Repositories\Interfaces\RepositoryInterface;

interface SettingInterface extends RepositoryInterface
{
}
